# Wobbleshrimp

A [Wobbledogs](https://wobbledogs.com/) fan game/shrimp care simulator for
Gameboy Color licensed under the [BSD 3-clause license](LICENSE).

## Outline

### Modal Views
In this game you take care of a tank of shrimp possibly around twenty to thirty
of them. Gameplay takes place over several screens:

"Tank View" where an overview and selection of the next mode "Shrimp View" can
be accessed. Basically a menu with a scrolling viewport horizontally. Possibly
makes use of the Gameboy's "window" feature to display overall information on
the bottom part of the screen. The scrolling tank will have the top row
dedicated to shrimp tiles (multiple stages of growth) and pre-rendered tank
decorations (multiple stages of development). An arrow cursor will be overlaid
on the screen with a sprite when the 'A' button is pressed. This is called
"Shrimp Select Mode" and can be exited with 'B', or go to "Shrimp View" with an
'A' press. Different modes should have their code stored in `ROMX`.

There exists the possibility for more than one shrimp tank.

### Shrimp Genetics & Gameplay
Shrimps can be various colors and have different genes like preferring different
pH levels. Different genes can be obtained by selectively feeding shrimp
different kinds of algae. Gameplay consists of balancing meters and values.
Other options for gameplay might be simply a screensaver with settings that can
be changed. Sort of a demo for the love of shrimp and such.

### Other Pets & Sound Design
Sounds should either be made with the onboard Gameboy synth, or with royalty
free woodwind/brass instruments. Possible plans for other pets like snails and
other critters. Valonia Ventricosa, a large unicellular organism can be fed to
the shrimp.

## To Do List
* [ ] Graphics
  * [ ] Tank graphics with alternate palette for clogged with algae.
  * [ ] Up-close shrimp graphics or photographs.
  * [ ] Tiles all on one PNG or two for easier loading.
  * [ ] Silly font with miniature shrimp gfx.
  * [ ] Other creatures.
    * [x] Snail to clean algae.
    * [ ] Unicellular organisms (Valonia Ventricosa).
* [ ] Sound
  * [ ] Sound effect driver.
  * [ ] Beeps for navigating.
* [-] Library routines for bank switching to different modes.
* [ ] HDMA copy routines for loading data into VRAM and possibly loading/saving
  to SRAM.
* [ ] Random number generator either using an LFSR or a block of pre-generated
  random numbers.