; vim:tw=80:ts=4

INCLUDE "gamestate.inc"
INCLUDE "hardware.inc"
INCLUDE "errcode.inc"

; ******************************************************************************
; Game State and Save/Load Routines.
; ******************************************************************************

SECTION "SRAM Routines", ROMX

; Initializes tank state.
InitTankState::
	ld a, BANK("Tank State") ; Bank in tank state
	ld [rRAMB], a
	ld hl, BeginTankState
	ld b, EndTankState - BeginTankState
	jp Zmem8

; Saves the game state to SRAM.
SaveState::
	di
	ld a, CART_SRAM_ENABLE   ; Enable cart SRAM.
	ld [rRAMG], a
	ld a, BANK("Save File")  ; Bank in save file.
	ld [rRAMB], a
	ld a, BANK("Tank State") ; Bank in tank state.
	ldh [rSVBK], a
	ld de, SaveString        ; Copy magic save string.
	ld hl, ssMagic
	ld b, SaveString.end - SaveString
	rst CopyMem8
	ld a, SAVE_VERSION       ; Write save version.
	ld [hli], a              ; HL is preserved from previous call to CopyMem8.
	ld de, BeginTankState    ; Copy tank state to save file.
	ld b, EndTankState - BeginTankState
	rst CopyMem8
	ld a, CART_SRAM_DISABLE  ; Disable cart SRAM.
	ld [rRAMG], a
	ld a, ECSUCCESS          ; Set success code and return.
	ld [wbErrCode], a
	reti

; Loads the game state from SRAM.
; Sets wbErrCode on return.
LoadState::
	di
	ld a, CART_SRAM_ENABLE   ; Enable cart SRAM.
	ld [rRAMG], a
	ld a, BANK("Save File")  ; Bank in save file.
	ld [rRAMB], a
	ld a, BANK("Tank State") ; Bank in tank state.
	ldh [rSVBK], a
	ld de, SaveString        ; Compare magic save string.
	ld hl, ssMagic
	ld bc, SaveString.end - SaveString
	call CmpStr16
	jr z, :+                 ; Return ECNOSAVE if save magic wrong.
		ld a, CART_SRAM_DISABLE
		ld [rRAMG], a
		ld a, ECNOSAVEFILE
		ld [wbErrCode], a
		reti
	:
	ld a, [sbVersion]        ; Compare save versions.
	cp SAVE_VERSION
	jr z, :+
		ld a, CART_SRAM_DISABLE
		ld [rRAMG], a
		ld a, ECBADSAVEVER
		ld [wbErrCode], a
		reti
	:
	ld de, BeginSaveData     ; Copy data.
	ld hl, BeginTankState
	ld b, EndSaveData - BeginSaveData
	rst CopyMem8
	ld a, CART_SRAM_DISABLE  ; Disable cart SRAM.
	ld [rRAMG], a
	ld a, ECSUCCESS          ; Set success code and return.
	ld [wbErrCode], a
	reti

; 'SAVEFILE' in ascii(7)
SaveString:
	db $53, $41, $56, $45, $46, $49, $4C, $45
.end:

SECTION "Tank State", WRAMX

BeginTankState::
wbShrimpCount:: db
wsShrimpData::  ds SHRMP_MAX_COUNT * sizeof_SHRMP_STATE
wbSnailCount::  db
wsSnailData::   ds SNAIL_MAX_COUNT * sizeof_SNAIL_STATE
EndTankState::

SECTION "Save File", SRAM

ssMagic:       ds SaveString.end - SaveString
sbVersion:     db
BeginSaveData:
sbShrimpCount: db
ssShrimpData:  ds SHRMP_MAX_COUNT * sizeof_SHRMP_STATE
sbSnailCount:  db
ssSnailData:   ds SNAIL_MAX_COUNT * sizeof_SNAIL_STATE
EndSaveData: