; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "font.inc"

; ******************************************************************************
; Text Renderer
; ******************************************************************************

SECTION "Text Renderer", ROM0

; Renders text to the GameBoy screen. This routine assume LCD is off!!
; @param de: Text data address.
; @param bc: Text data length.
; @param hl: Bank of text data.
RenderText::

	; Preserve our parameters while we set up.
	push hl
	push de
	push bc

	; Load proper palette.
	ld hl, BANK("Font Tiles")
	call ChangeBank
	ld hl, FontPal
	ld b, FontPal.end - FontPal
	call LoadCgbBgPalette

	; Set scroll position to zero.
	ld a, 0
	ldh [rSCY], a
	ldh [rSCX], a

	; Load font tiles.
	ld a, BANK("Font Tiles")
	ld [rROMB0], a
	ld bc, FontTiles.end - FontTiles
	ld de, FontTiles
	ld hl, $9000
	call CopyMem16

	; Clear screen buffer.
	ld hl, _SCRN0
	ld bc, $9BFF - _SCRN0
	call Zmem16

	; Set wbIndexer to zero
	ld a, 0
	ld [wbIndexer], a

	; Restore our parameters
	pop bc
	pop de
	pop hl

	call ChangeBank

	; Do render
	ld hl, _SCRN0
.nextChar:
	ld a, [de]     ; Load char.
	cp "%"         ; Compare vs terminator.
	ret z
	cp "\n"        ; Compare vs escape chars.
	jr z, .newLine
	ld [hli], a    ; Copy char to VRAM if not escape char.
	ld a, [wbIndexer]
	inc a
	cp SCRN_X_B
	jr nz, :+ ; Skip wrapping

		; Make sure we wrap every 20 bytes
		push bc
		ld b, a
		ld b, SCRN_VX_B - SCRN_X_B
		rst Zmem8
		ld a, 0
		pop bc

	:
	ld [wbIndexer], a
	inc de
	dec bc
	ld a, b
	or c
	jr nz, .nextChar
	ret

.newLine:
	; HL += (SCRN_X_B - [wbIndexer]) + (SCRN_VX_B - SCRN_X_B)
	push de ; Preserve DE.
	ld a, [wbIndexer]        ; Subtract wbIndexer from SCRN_X_B.
	ld e, a
	ld a, SCRN_X_B
	sub e
	add SCRN_VX_B - SCRN_X_B ; Add size of space offscreen.
	rst Add8to16             ; Add to HL.
	pop de

	; Resume
	inc de                   ; Get next char.
	dec bc                   ; Decrement count.
	ld a, 0                  ; Reset line indexer.
	ld [wbIndexer], a
	jr .nextChar
	ret