; vim:tw=80:ts=4

INCLUDE "hardware.inc"

; ******************************************************************************
; Crash Handler
;
; Clears BG RAM and loads a sad face tile and places it in the upper corner of
; the screen to indicate a crash.
; ******************************************************************************

SECTION "Crash Handler", ROM0

CrashHandler::

	; Disable interrupts, just in case
	di

	; Turn LCD off.
	call WaitUntilVBlank
	ld a, LCDCF_OFF
	ldh [rLCDC], a

	; Load sad face tile.
	ld de, CrashIndicatorTile
	ld hl, $9000
	ld b, CrashIndicatorTile.end - CrashIndicatorTile
	rst CopyMem8

	; Put it in the tilemap.
	ld a, 0
	ld [_SCRN0], a

	; Reset scroll positions.
	ldh [rSCY], a
	ldh [rSCX], a

	; Load proper palettes.
	ld hl, BANK("Font Tiles")
	ld a, l
	ld [rROMB0], a
	ld a, h
	ld [rROMB1], a
	ld hl, FontPal
	ld b, FontPal.end - FontPal
	call LoadCgbBgPalette

	; Turn LCD back on.
	ld a, LCDCF_ON | LCDCF_BGON | LCDCF_BG8800 | LCDCF_WIN9800 | LCDCF_OBJOFF
	ldh [rLCDC], a

	; Re-enable interrupts so we don't waste power
	ld a, IEF_VBLANK
	ldh [rIE], a
	ei

	; Softbreak, halt & loop infinitely
	ld b, b
	:
		halt
		nop
		jr :-

; **************************************************************************
; Crash Indicator Tile
; **************************************************************************

CrashIndicatorTile:
	dw `00000000
	dw `00300300
	dw `00300300
	dw `00300300
	dw `00000000
	dw `00333300
	dw `03000030
	dw `00000000
.end: