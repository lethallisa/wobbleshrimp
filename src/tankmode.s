; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "tankmode.inc"
INCLUDE "gamestate.inc"

; ******************************************************************************
; Routines for Shrimp Tank Game Mode
; ******************************************************************************

SECTION "Shrimp Tank Mode", ROMX

TankModeMain::

	; Reset OAM bump allocator.
	ld a, 0
	ldh [hbSOamCounter], a

	; Render sprites based on mode state.
	ld a, [wbShrimpCount]
	cp 0              ; Skip rendering if there are no shrimp.
	jr z, .skipShrimp
	ld [wbIndexer], a ; Put count into indexer.
	ld hl, wsShrimpData
	:
		ld a, [hli]        ; Get Y pos
		ld c, a
		ld a, [hli]        ; Get X pos.
		ld b, a
		ld a, [hli]        ; Get flags.
		ld d, a
		push hl            ; Preserve HL.
		call RenderShrimp
		pop hl
		ld bc, NAME_LENGTH ; Jump over name field since we don't use it for
		add hl, bc         ; rendering.
		ld a, [wbIndexer]  ; Decrement count.
		dec a
		ld [wbIndexer], a
		jr nz, :-
.skipShrimp:
	ld a, [wbSnailCount]
	cp 0              ; Skip rendering if there are no snails.
	jr z, .skipSnail
	ld [wbIndexer], a ; Put count into indexer.
	ld hl, wsSnailData
	:
		ld a, [hli]        ; Get Y pos.
		ld c, a
		ld a, [hli]        ; Get X pos.
		ld b, a
		ld a, [hli]        ; Get flags.
		ld d, a
		push hl            ; Preserve HL.
		call RenderSnail
		pop hl
		ld bc, NAME_LENGTH ; Jump over name field since we don't use it for
		add hl, bc         ; rendering
		ld a, [wbIndexer]  ; Decrement count.
		dec a
		ld [wbIndexer], a
		jr nz, :-
.skipSnail:

	; Clear the rest of OAM out.
	call OamClearer

	; Wait for VBLANK and copy to OAM using DMA.
	rst RstWaitUntilVBlank
	call HramDmaCopyOam

	; Get input from keypad.
	call DoInput

	; Reado loop if no keys pressed.
	ldh a, [hCurKeys]
	cp 0
	jr z, TankModeMain

	; Update game state based on input.
	; TODO: Simulation tick procedure.

	; Loop to next frame.
	jp TankModeMain

; @param b: X position.
; @param c: Y position.
; @param d: flags: sprite flip and shrimp color.
RenderShrimp:
	call OamAllocator      ; Call OAM allocator to get initial position in SOAM.

	; Add up offsets of left half.
	ld a, OAM_Y_OFS        ; Add Y offset.
	add c
	ld [hli], a
	ld a, OAM_X_OFS        ; Add X offset.
	add b
	ld [hli], a

	; Write tile ID of left half.
	bit TANKRNDRB_FLIP, d  ; If flip bit is set then write tail tile...
	jr z, :+
		ld a, SHRMPTILE_TAIL
		jr :++
	:                      ; ...Else write head tile.
		ld a, SHRMPTILE_HEAD
	:
	ld [hli], a

	; Call out to attrs writer.
	call WriteShrimpOamAttrs

	; Instead of calling OamAllocator increment its counter.
	ldh a, [hbSOamCounter]
	inc a
	ldh [hbSOamCounter], a

	; Add up offsets of right half.
	ld a, OAM_Y_OFS        ; Add Y offset.
	add c
	ld [hli], a
	ld a, OAM_X_OFS + 8    ; Add X offset + offset from left half.
	add b
	ld [hli], a

	; Write tile ID of right half.
	bit TANKRNDRB_FLIP, d ; If flip bit is set then write head tile...
	jr z, :+
		ld a, SHRMPTILE_HEAD
		jr :++
	:                     ; ...Else write tail tile.
		ld a, SHRMPTILE_TAIL
	:
	ld [hli], a

	; NOTE: Falls through here to WriteShrimpOamAttrs, which handles the `ret`
	; for this function. Effectively avoiding a tail-call.
	;jr WriteShrimpOamAttrs

WriteShrimpOamAttrs:
	bit TANKRNDRB_GREY, d ; Draw shrimp in pink unless they're in grey.
	jr nz, :+
		ld a, SHRMPPAL_PINK & OAMF_PALMASK
		ld e, a           ; Store palette to E.
		jr .flip
	:
	ld a, SHRMPPAL_GREY & OAMF_PALMASK
	ld e, a               ; Store palette to E.
.flip:
	ld a, 0               ; Unset all attrs.
	bit TANKRNDRB_FLIP, d ; Flip tile accross Y axis if set.
	jr z, :+              ; Do nothing if bit is zero.
		ld a, OAMF_XFLIP
	:
	or e                  ; Mask attrs | palette.
	ld [hli], a           ; Store to OAM.
	ret

; @param b: X position.
; @param c: Y position.
; @param d: flags: sprite flip and shrimp color.
RenderSnail:
	call OamAllocator      ; Get start position in shadow OAM.

	; Add up offsets of left half.
	ld a, OAM_Y_OFS        ; Add up Y position.
	add c
	ld [hli], a
	ld a, OAM_X_OFS        ; Add up X position.
	add b
	ld [hli], a

	; Write tile ID for left half.
	bit TANKRNDRB_FLIP, d
	jr z, :+              ; Write shell tile if not flipped...
		ld a, SNAILTILE_SHELL
		jr :++
	:                     ; ...Else write head tile.
		ld a, SNAILTILE_HEAD
	:
	ld [hli], a

	; Call out to WriteSnailOamAttrs.
	call WriteSnailOamAttrs

	; Increment SOAM counter instead of calling OamAllocator again.
	ldh a, [hbSOamCounter] ; Increment SOAM counter.
	inc a
	ldh [hbSOamCounter], a

	; Add up offsets of right half
	ld a, OAM_Y_OFS        ; Add up Y position.
	add c
	ld [hli], a
	ld a, OAM_X_OFS + 8    ; Add up X position.
	add b
	ld [hli], a

	; Write tile ID for right half.
	bit TANKRNDRB_FLIP, d
	jr z, :+              ; Write head tile if not flipped...
		ld a, SNAILTILE_HEAD
		jr :++
	:                     ; ...Else write shell tile.
		ld a, SNAILTILE_SHELL
	:
	ld [hli], a

	; NOTE: Fall through to WriteSnailOamAttrs avoiding tail-call.
	;jr WriteSnailOamAttrs

WriteSnailOamAttrs:
	ld e, SNAILPAL         ; Load palette into E.
	ld a, 0                ; Set bits in A depending on values set in D.
	bit TANKRNDRB_FLIP, d
	jr z, :+
		ld a, OAMF_XFLIP
	:
	or e                   ; A |= E.
	ld [hli], a            ; Write into SOAM.
	ret

; y = (2 * x) + 1
; @param a: X
; @return a: Y
ProcessSlope:
	add a ; Add A to itself instead of implenting multiplication.
	inc a ; Add base (1).
	ret
