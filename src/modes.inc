; vim:tw=80:ts=4:ft=asm

; ******************************************************************************
; Mode Macros and Definitions
; ******************************************************************************

; Values for change mode jump table.
DEF MODE_TANK   EQU 0
DEF MODE_DETAIL EQU 1
DEF MODE_PAUSE  EQU 2
DEF MODE_MAX    EQU MODE_PAUSE