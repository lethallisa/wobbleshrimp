; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "interrupts.inc"
INCLUDE "input.inc"

; ******************************************************************************
; Toolbox
; Routines that help make other code easier to write.
; ******************************************************************************

SECTION "Toolbox", ROM0

; Waits until VBLANK using interrupts.
; Clobbers: af
WaitUntilVBlankI::
	:
		halt ; Wait for interrupt.
		nop
		ldh a, [hIntFlag] ; Loop if interrupt is not INT_VBLANK.
		cp INT_VBLANK
		jr nz, :-
	ld a, 0 ; Clear interrupt flag.
	ldh [hIntFlag], a
	ret

; Waits until VBLANK using software.
; Clobbers: af
WaitUntilVBlank::
	ldh a, [rLY]
	cp SCRN_Y
	jr z, WaitUntilVBlank
	ret

; Waits until VBLANK is done using software.
; Clobbers: af
WaitFinishVBlank::
	ldh a, [rLY]
	cp SCRN_Y
	jr nz, WaitFinishVBlank
	ret

; Changes the current ROMX bank. This particular routine is specific to MBC5.
; @param hl: Bank to change to.
ChangeBank::
	ld a, l
	ld [rROMB0], a
	ld a, h
	ld [rROMB1], a
	ret

; Copies data from one place to another (16-bit addressing).
; @param de: Start address.
; @param hl: Destination address.
; @param bc: Data length.
; Clobbers: af
CopyMem16::
	ld a, [de]
	ld [hli], a
	inc de
	dec bc
	ld a, b
	or c
	jr nz, CopyMem16
	ret

; Zeros out memory (16-bit addressing).
; @param hl: Address to start clearing from.
; @param bc: Length (# of bytes) to zero out.
; Clobbers: af
Zmem16::
	ld a, 0
	ld [hli], a
	dec bc
	ld a, b
	or c
	jr nz, Zmem16
	ret

; @param de: string1 addr
; @param hl: string2 addr
; @param bc: bytes to compare
; @return zf: set if equal, unset if not.
CmpStr16::
	ld a, [de]  ; Compare bytes.
	cp a, [hl]
	ret nz
	inc de      ; Get next bytes.
	inc hl
	dec bc      ; Decrement counter.
	ld a, b
	or c
	jr nz, CmpStr16
	ret         ; Returning, Z set.

SECTION "VRAM Copy Routine", ROM0

; Chains together loading of sprites from different memory locations into VRAM.
; @param de: start of sprite data.
; @param b: length of sprite data.
BeginLoadTileToVramB0::
	push de               ; Preserve DE.
	ld de, $8000          ; Set Bank 0.
	jr LoadTileToVramFirst
BeginLoadTileToVramB1::
	push de               ; Preserve DE.
	ld de, $8800          ; Set Bank 1.
	jr LoadTileToVramFirst
BeginLoadTileToVramB2::
	push de               ; Presereve DE.
	ld de, $9000          ; Set Bank 2.
	; Fall through
LoadTileToVramFirst:
	ld a, e               ; Store initial position in wwIndexer.
	ld [wwIndexer], a
	ld a, d
	ld [wwIndexer + 1], a
	pop de                ; Restore DE.
LoadTileToVramNext::
	ld a, [wwIndexer]     ; Reload last position from wwIndexer.
	ld l, a
	ld a, [wwIndexer + 1]
	ld h, a
	rst CopyMem8          ; Do copy operation.
	ld a, l               ; Preserve wwIndexer for next call.
	ld [wwIndexer], a
	ld a, h
	ld [wwIndexer + 1], a
	ret

SECTION "CGB Routines", ROM0

; Loads a palette block into the background palette.
; @param hl: start of palette data.
; @param b: length of palette data in bytes.
LoadCgbBgPalette::
	ld a, BCPSF_AUTOINC ; Reset start index and set autoinc bit.
	ldh [rBCPS], a
LoadCgbBgPaletteNext::
		ld a, [hli]     ; Copy palette data into BCPD.
		ldh [rBCPD], a
		dec b
	jr nz, LoadCgbBgPaletteNext
	ret

; Loads a palette block into the foreground palette.
; @param hl: start of palette data.
; @param b: length of palette data in bytes.
LoadCgbFgPalette::
	ld a, OCPSF_AUTOINC ; Reset start index and set autoinc bit.
	ldh [rOCPS], a
LoadCgbFgPaletteNext::
		ld a, [hli]     ; Copy palette data into OCPD.
		ldh [rOCPD], a
		dec b
	jr nz, LoadCgbFgPaletteNext
	ret

; Switches the current CGB speed.
SwitchSpeed::
	di
	push af
	ldh a, [rIE]        ; Store rIE.
	ld [wbIndexer], a
	ld a, 0             ; Erase rIE.
	ldh [rIE], a
	ld a, P1F_GET_NONE  ; Set joypad register as per recommended.
	ldh [rP1], a
	ld a, KEY1F_PREPARE ; Arm switch.
	ldh [rKEY1], a
	stop                ; Perform switch.
	ld a, [wbIndexer]   ; Restore rIE.
	ldh [rIE], a
	pop af
	reti

SECTION "Toolbox WRAM", WRAM0
wbErrCode:: db ; Error code to return from routines.
wbIndexer:: db ; Byte sized general indexer.
wwIndexer:: dw ; Word sized general indexer.