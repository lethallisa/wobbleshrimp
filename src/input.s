; vim:tw=80:ts=4

INCLUDE "hardware.inc"

; **************************************************************************
; Input Routine
; Modified from public domain code.
; **************************************************************************

SECTION "Input Routines", ROM0

DoInput::
	; Poll buttons.
	ld a, P1F_GET_BTN
	call .oneNybble
	ld b, a ; B7-4 = 1; B3-0 = unpressed buttons

	; Poll dpad.
	ld a, P1F_GET_DPAD
	call .oneNybble
	swap a   ; A3-0 = unpressed directions; A7-4 = 1
	xor b    ; A = pressed buttons + directions
	ld b, a  ; B = pressed buttons + directions

	; Release controller
	ld a, P1F_GET_NONE
	ldh [rP1], a

	; Combine with previous hCurKeys to make hNewKeys
	ldh a, [hCurKeys]
	xor b ; A = keys that changed state
	and b ; A = keys that changed to pressed
	ldh [hNewKeys], a
	ld a, b
	ldh [hCurKeys], a
	ret

.oneNybble:
	ldh [rP1], a   ; Switch the key matrix
	call :+        ; Burn 10 cycles calling a known return.
	ldh a, [rP1]   ; Ignore values while waiting for debounce.
	ldh a, [rP1]
	ldh a, [rP1]   ; This read counts
	or $F0      ; A7-4 = 1; A3-0 = unpressed keys
	:
		ret


; **************************************************************************
; Variables Used by Input Processor
; **************************************************************************

SECTION "Input Variables", HRAM
hCurKeys:: db
hNewKeys: db
hInputDelay:: db