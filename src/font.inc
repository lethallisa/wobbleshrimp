; vim:ft=asm:tw=80:ts=4

; ******************************************************************************
; Charmap for Font and Associated Definitions
; ******************************************************************************

CHARMAP " ", 0
CHARMAP "A", 1
CHARMAP "B", 2
CHARMAP "C", 3
CHARMAP "D", 4
CHARMAP "E", 5
CHARMAP "F", 6
CHARMAP "G", 7
CHARMAP "H", 8
CHARMAP "I", 9
CHARMAP "J", 10
CHARMAP "K", 11
CHARMAP "L", 12
CHARMAP "M", 13
CHARMAP "N", 14
CHARMAP "O", 15
CHARMAP "P", 16
CHARMAP "Q", 17
CHARMAP "R", 18
CHARMAP "S", 19
CHARMAP "T", 20
CHARMAP "U", 21
CHARMAP "V", 22
CHARMAP "W", 23
CHARMAP "X", 24
CHARMAP "Y", 25
CHARMAP "Z", 26
CHARMAP ".", 27
CHARMAP ",", 28
CHARMAP "'", 29 ; single quote
CHARMAP "\"", 30 ; double quote
CHARMAP "!", 31
CHARMAP "?", 32
CHARMAP "$", 33 ; double bang
CHARMAP "1", 34
CHARMAP "2", 35
CHARMAP "3", 36
CHARMAP "4", 37
CHARMAP "5", 38
CHARMAP "6", 39
CHARMAP "7", 40
CHARMAP "8", 41
CHARMAP "9", 42
CHARMAP "0", 43
CHARMAP "(", 44
CHARMAP ")", 45
CHARMAP "-", 46
CHARMAP ":", 47
CHARMAP "\n", $7F
CHARMAP "%", $FF