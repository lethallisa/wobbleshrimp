; vim:tw=80:ts=4

INCLUDE "hardware.inc"

; ******************************************************************************
; Sound handling code.
; Init code based on:
; https://github.com/pinobatch/libbet/blob/master/src/audio.z80
; ******************************************************************************

SECTION "Audio Routines", ROM0

InitAudio::
	ld a, AUDENA_ON      ; Enable audio.
	ldh [rAUDENA], a
	ld a, $FF            ; Stereo.
	ldh [rAUDTERM], a
	ld a, $77
	ldh [rAUDVOL], a
	ld a, AUD1SWEEP_DOWN ; Disable sweep.
	ldh [rAUD1SWEEP], a

	; Silence channels.
	ld a, 0
	ldh [rAUD1ENV], a
	ldh [rAUD2ENV], a
	;ld a, AUD3LEVEL_MUTE
	ldh [rAUD3LEVEL], a
	ldh [rAUD4ENV], a
	ld a, $80
	ldh [rAUD1HIGH], a
	ldh [rAUD2HIGH], a
	ldh [rAUD3HIGH], a
	ldh [rAUD4GO], a
	ret