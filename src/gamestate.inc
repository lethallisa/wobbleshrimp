; vim:tw=80:ts=4:ft=asm

; ******************************************************************************
; Size Definitions and Macros for Game State Structures
; ******************************************************************************

DEF SAVE_VERSION EQU 0

DEF NAME_LENGTH     EQU 8
DEF SHRMP_MAX_COUNT EQU 16
DEF SNAIL_MAX_COUNT EQU 2

RSRESET
DEF SHRMP_YPOS         RB 1
DEF SHRMP_XPOS         RB 1
DEF SHRMP_FLAGS        RB 1
DEF SHRMP_NAME         RB NAME_LENGTH
DEF sizeof_SHRMP_STATE RB 0

RSRESET
DEF SNAIL_YPOS         RB 1
DEF SNAIL_XPOS         RB 1
DEF SNAIL_FLAGS        RB 1
DEF SNAIL_NAME         RB NAME_LENGTH
DEF sizeof_SNAIL_STATE RB 0

