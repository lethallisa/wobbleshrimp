; vim:tw=80:ts=4:ft=asm

; ******************************************************************************
; Interrupt Handler Macros and Defines
; ******************************************************************************

DEF INT_VBLANK EQU $01
DEF INT_STAT   EQU $02
DEF INT_TIMER  EQU $04
DEF INT_SERIAL EQU $08
