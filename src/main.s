; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "input.inc"
INCLUDE "modes.inc"
INCLUDE "errcode.inc"
INCLUDE "stack.inc"

; ******************************************************************************
; Setup ROM Header
;
; Basically create an empty space to be filled by rgbfix later.
; ******************************************************************************

SECTION "Header", ROM0[$0100]

	jp EntryPoint
	ds $150 - @, 0

	db "Wobbleshrimp build: ", __ISO_8601_UTC__, 0

; ******************************************************************************
; ROM Entry Point
;
; Serves as initialization of the hardware.
; ******************************************************************************

SECTION "Entry Point", ROM0

EntryPoint::
	ld hl, Stack      ; Clear stack.
	ld b, STACK_SIZE
	rst Zmem8
	ld sp, Stack + STACK_SIZE - 1 ; Set stack pointer.
	ld a, 0
	ldh [hIntFlag], a ; Init interrupt flag.
	ld a, IEF_VBLANK  ; Enable VBLANK interrupts.
	ldh [rIE], a
	ei ; enable interrupts while we display the license screen.

	; Turn off sound.
	ld a, AUDENA_OFF
	ldh [rNR52], a

	; Show license screen.
	ld hl, BANK("License Screen")
	ld a, l
	ld [rROMB0], a
	ld a, h
	ld [rROMB1], a
	call ShowLicense

	di ; Disable interrupts while we do setup.

	; Initialize variables.
	ld a, 0                 ; Initialize OAM data counter.
	ldh [hbSOamCounter], a

	ldh a, [rDIV]           ; Initialize RNG seed.
	ldh [hwSeed], a

	ld a, INPUT_DELAY_COUNT ; Initialize input delay.
	ldh [hInputDelay], a

	; Wait for VBLANK and shut off LCD.
	call WaitUntilVBlank
	ld a, LCDCF_OFF
	ldh [rLCDC], a

	; Clear Tile VRAM.
	ld hl, $8000
	ld bc, $97FF - $8000
	call Zmem16

	; Clear OAM.
	ld b, OAM_COUNT * sizeof_OAM_ATTRS
	ld hl, _OAMRAM
	rst Zmem8

	; Clear OAM copy in WRAM.
	ld b, wShadowOam.end - wShadowOam
	ld hl, wShadowOam
	rst Zmem8

	; Copy OAM DMA routine into HRAM.
	ld b, DmaCopyOam.end - DmaCopyOam
	ld hl, HramDmaCopyOam
	ld de, DmaCopyOam
	rst CopyMem8

	; Load DMG palettes because it's basically free, but our game won't function
	; with these.
	ld a, %11100100
	ldh [rBGP], a
	ldh [rOBP0], a
	ld a, %00011011
	ldh [rOBP1], a

	; Turn LCD back on & enable interrupts.
	ld a, LCDCF_ON | LCDCF_OBJON | LCDCF_BGON
	ldh [rLCDC], a
	ei

	; Load game state from save file.
	ld hl, BANK("SRAM Routines")
	call ChangeBank
	call LoadState
	ld a, [wbErrCode]
	cp ECSUCCESS
	jr z, :+++
		cp ECNOSAVEFILE
		jr nz, :+
			; TODO: Print out a notification of what went wrong here. That there
			; is no save file and one is to be created by pressing start.
			call InitTankState
			call SaveState
			jr :+++
		:
		cp ECBADSAVEVER
		jr nz, :+
			; TODO: Print out a message here that indicates that the save file
			; is the wrong version and ask to overwrite it or not.
			call InitTankState
			call SaveState
			jr :++
		:
		rst RstCrash
	:

	; Change to initial mode.
	ld a, MODE_TANK
	ld [wbCurMode], a
	jp ChangeMode