; vim:tw=80:ts=4

INCLUDE "hardware.inc"

; ******************************************************************************
; OAM DMA Routines
; ******************************************************************************

SECTION "OAM Management Routines", ROM0

; Copies SOAM into OAM using DMA.
; This routine should NOT be called directly and should instead be loaded into
; HRAM, and called via HDmaCopyOam.
DmaCopyOam::
	ld a, HIGH(wShadowOam)
	ldh [rDMA], a   ; start DMA transfer (starts right after instruction)
	ld a, 40        ; delay for a total of 4×40 = 160 cycles
	:
		dec a       ; 1 cycle
		jr nz, :-   ; 3 cycles
	ret
.end::

; Allocate space in ShadowOAM.
; @return hl: Allocated address into OAM.
OamAllocator::
	; Offset into OAM.
	ld hl, wShadowOam
	ldh a, [hbSOamCounter]
	add a                 ; Multiply by 4.
	add a
	jr nc, :+              ; Crash if overflow.
		rst RstCrash
	:
	add l                  ; Add HL+A.
	ld l, a
	adc h
	sub l
	ld h, a
	ldh a, [hbSOamCounter] ; Incrememnt counter.
	inc a
	ldh [hbSOamCounter], a
	ret

; Clears the remaining space in ShadowOAM.
OamClearer::
	call OamAllocator
	:
		xor a
		REPT 4
			ld [hli], a
		ENDR
		ldh a, [hbSOamCounter]
		inc a
		ldh [hbSOamCounter], a
		cp OAM_COUNT
		jr nz, :-
	ret

SECTION "Reserved HRAM Area For OAM DMA", HRAM

HramDmaCopyOam::
	ds DmaCopyOam.end - DmaCopyOam

SECTION "OAM Allocator Counter Byte", HRAM

hbSOamCounter:: db

; ******************************************************************************
; ShadowOAM - OAM Copy in WRAM
; ******************************************************************************

SECTION "Shadow OAM", WRAM0, ALIGN[8]

wShadowOam::
	ds OAM_COUNT * sizeof_OAM_ATTRS
.end::
;wbSOamCounter:: db

; ******************************************************************************
; Sprite Data
; ******************************************************************************

SECTION "Menu Sprites", ROMX

MenuArrow::
	dw `00000000
	dw `01230000
	dw `00003300
	dw `00000030
	dw `01233333
	dw `00000030
	dw `00003300
	dw `01230000
.end::

MenuOk::
	dw `00000000
	dw `01001001
	dw `20202020
	dw `30303300
	dw `30303030
	dw `30303003
	dw `20202002
	dw `01001001
.end::

SECTION "Tank Mode Sprites", ROMX

ShrimpSprites::
	INCBIN "sprites/shrimp.2bpp"
.end::

ShrimpPal::
	INCBIN "sprites/shrimp.pal"
.end::

SnailSprites::
	INCBIN "sprites/snail.2bpp"
.end::

SnailPal::
	INCBIN "sprites/snail.pal"
.end::

BubblesSprites::
	INCBIN "sprites/bubbles.2bpp"
.end::

BubblesPal::
	INCBIN "sprites/bubbles.pal"
.end::