; vim:ft=asm:tw=80:ts=4

; ******************************************************************************
; Error Code Defines
; ******************************************************************************

DEF ECSUCCESS    EQU $00
DEF ECFAIL       EQU $FF
DEF ECNOSAVEFILE EQU $01
DEF ECBADSAVEVER EQU $02
