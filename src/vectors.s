; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "interrupts.inc"

; ******************************************************************************
; Reset Vectors
; Most of these are used to call utility functions.
; ******************************************************************************

SECTION "Rst $00", ROM0[$0000]

; Handle jumps to $0000 as crashes.
RstNull::
RstCrash::
	ld b, b ; Softbreak.
	di
	nop
	nop
	jp CrashHandler

SECTION "Rst $10", ROM0[$0010]

; Handles VBLANK using interrupts.
RstWaitUntilVBlank::
	jp WaitUntilVBlankI

SECTION "Rst $18", ROM0[$0018]

; Zeros out memory.
; @param hl: Address to start clearing from.
; @param b:  Length (# of bytes) to zero out.
; Clobbers: af
; When called by Memset8:
; @param a: Byte to set.
Zmem8::
	ld a, 0
Memset8::
	ld [hli], a
	dec b
	jr nz, Memset8
	ret

SECTION "Rst $20", ROM0[$0020]

; Copies memory from one place to another.
; Input:
; @param de: Start address.
; @param hl: Destination address.
; @param b:  Data length.
; Clobbers: af
CopyMem8::
	ld a, [de]
	ld [hli], a
	inc de
	dec b
	jr nz, CopyMem8
	ret

SECTION "Rst $28", ROM0[$0028]

; Jumps to the value stored in [HL].
; @param hl: Address of pointer to jump to.
JpPtr::
	ld a, [hli]
	ld h, [hl]
	ld l, a
	jp hl

SECTION "Rst $30", ROM0[$0030]

; Gets the value stored at address stored in HL.
; @param hl: Address to dereference.
; @param hl: Output of dereference.
DereferenceHL::
	ld a, [hli]
	ld h, [hl]
	ld l, a
	ret

SECTION "Rst $38", ROM0[$0038]

; Does an 8-to-16 bit addition.
; @param a: 8 bit part to add.
; @param hl: 16 bit part to add.
; @return hl: result.
Add8to16::
	add l
	ld l, a
	adc h
	sub l
	ld h, a
	ret

; ******************************************************************************
; Interrupt Handlers
; ******************************************************************************

SECTION "VBlank Handler", ROM0[$0040]

VBlankHandler:
	di
	ld a, INT_VBLANK
	ldh [hIntFlag], a
	reti

SECTION "LCDC Handler", ROM0[$0048]

LCDCHandler:
	di
	ld a, INT_STAT
	ldh [hIntFlag], a
	reti

SECTION "Timer Handler", ROM0[$0050]

TimerHandler:
	di
	ld a, INT_TIMER
	ldh [hIntFlag], a
	reti

SECTION "Serial Handler", ROM0[$0058]

SerialHandler:
	di
	ld a, INT_SERIAL
	ldh [hIntFlag], a
	reti

SECTION "Joypad Handler", ROM0[$0060]

JoypadHandler:
	reti

SECTION "Interrupt Flag", HRAM
hIntFlag:: db ; Flag used by interrupt handlers to indicate interrupt type.