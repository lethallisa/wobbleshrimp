; vim:tw=80:ts=4

; ******************************************************************************
; Random Number Generator
; Based on Xorshift Z80 @
; http://www.retroprogramming.com/2017/07/xorshift-pseudorandom-numbers-in-z80.html
; ******************************************************************************

SECTION "Random Number Generator", ROM0

Xrng::
	ldh a, [hwSeed]     ; Get seed value from HRAM.
	ld l, a
	ldh a, [hwSeed + 1]
	;ld h, a
	rra
	ld a, l
	rra
	xor h
	ld h, a
	ld a, l
	rra
	ld a, h
	rra
	xor l
	ld l, a
	xor h
	;ld h, a
	ldh [hwSeed + 1], a ; Store seed value to HRAM.
	ld a, l
	ldh [hwSeed], a
	ret

SECTION "Random Number Generator Seed", HRAM

hwSeed:: dw