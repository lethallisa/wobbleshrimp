; vim:tw=80:ts=4:ft=asm

; ******************************************************************************
; Tank Game Mode Includes
; ******************************************************************************

; Renderer flags.
DEF TANKRNDRF_PINK EQU $00
DEF TANKRNDRF_GREY EQU $01
DEF TANKRNDRF_FLIP EQU $02
DEF TANKRNDRB_GREY EQU 0
DEF TANKRNDRB_FLIP EQU 1

; Tile and palette IDs.
DEF SHRMPTILE_HEAD  EQU $02
DEF SHRMPTILE_TAIL  EQU $03
DEF SNAILTILE_HEAD  EQU $04
DEF SNAILTILE_SHELL EQU $05
DEF SHRMPPAL_PINK   EQU $01
DEF SHRMPPAL_GREY   EQU $02
DEF SNAILPAL        EQU $03
