; vim:tw=80:ts=4

INCLUDE "stack.inc"

; ******************************************************************************
; Stack area in WRAM to preserve space in HRAM.
; ******************************************************************************

SECTION "Stack", WRAM0, ALIGN[8]

Stack::
	ds STACK_SIZE