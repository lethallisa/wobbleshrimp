; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "modes.inc"
INCLUDE "tankmode.inc"

; ******************************************************************************
; Routines for switching between modes.
; ******************************************************************************

SECTION "Mode Switching Routines", ROM0

ChangeMode::

	; Turn LCD off while we do our mode change.
	rst RstWaitUntilVBlank
	di
	ld a, LCDCF_OFF
	ldh [rLCDC], a

	; Switch to high speed mode.
	; NOTE: Actual change mode routines must reset speed back to normal.
	call SwitchSpeed

	; Handle other modes using a jump table.
	ld a, [wbCurMode]        ; Get current mode number.
	cp MODE_MAX              ; Crash on mode # > maximum.
	jr c, :+
		rst RstCrash
	:
	add a                    ; Multiply by two to get word address.
	ld hl, ChangeModeJpTable ; Load jump table base into hl.
	rst Add8to16             ; Add a to hl.
	rst JpPtr                ; Jump to pointer at hl.

ChangeModeJpTable:
	dw ChangeModeTank
	dw ChangeModeDetail
	dw ChangeModePause

SECTION "Tank Mode Change Routine", ROM0

ChangeModeTank:

	; Load tank mode object tiles.
	ld hl, BANK("Menu Sprites")
	call ChangeBank
	ld de, MenuArrow
	ld b, MenuOk.end - MenuArrow
	call BeginLoadTileToVramB0
	ld hl, BANK("Tank Mode Sprites")
	call ChangeBank
	ld de, ShrimpSprites
	ld b, ShrimpSprites.end - ShrimpSprites
	call LoadTileToVramNext
	ld de, SnailSprites
	ld b, SnailSprites.end - SnailSprites
	call LoadTileToVramNext
	ld de, BubblesSprites
	ld b, BubblesSprites.end - BubblesSprites
	call LoadTileToVramNext

	; Load tank mode background tiles.
	ld hl, BANK("Tank Mode Tiles")
	call ChangeBank
	ld hl, TankTiles
	ld a, h
	ldh [rHDMA1], a
	ld a, l
	ldh [rHDMA2], a
	ld a, $90
	ldh [rHDMA3], a
	ld a, $0
	ldh [rHDMA4], a
	ld a, HDMA5F_MODE_GP | (((TankTiles.end - TankTiles) / $10) - 1)
	ldh [rHDMA5], a

	; Make sure we are done with HDMA transfer.
	:
		ldh a, [rHDMA5]
		bit HDMA5B_MODE, a
	jr z, :-

	; Load tank mode OBJ palettes.
	ld hl, BANK("Font Tiles")
	call ChangeBank
	ld hl, FontPal
	ld b, FontPal.end - FontPal
	call LoadCgbFgPalette
	ld hl, BANK("Tank Mode Sprites")
	call ChangeBank
	ld hl, ShrimpPal
	ld b, ShrimpPal.end - ShrimpPal
	call LoadCgbFgPaletteNext
	ld hl, SnailPal
	ld b, SnailPal.end - SnailPal
	call LoadCgbFgPaletteNext
	ld hl, BubblesPal
	ld b, BubblesPal.end - BubblesPal
	call LoadCgbFgPaletteNext

	; Load tank mode background palettes.
	ld hl, BANK("Tank Mode Tiles")
	call ChangeBank
	ld hl, TankPal
	ld b, TankPal.end - TankPal
	call LoadCgbBgPalette

	; Load tilemap.
	ld a, 0            ; Use wbIndexer to store X position in copy routine.
	ld [wbIndexer], a
	ldh [rVBK], a      ; Bank in VRAM bank 0.
	ld de, TankTileMap
	ld bc, TankTileMap.end - TankTileMap ; Use BC as size counter.
	call TankModeScrnCopy

	; Load attrmap.
	ld a, 1 ; Bank in VRAM bank 1.
	ldh [rVBK], a
	ld de, TankAttrMap
	ld bc, TankAttrMap.end - TankAttrMap
	call TankModeScrnCopy

	; Switch back to standard speed.
	call SwitchSpeed

	; Turn LCDC back on
	ld a, LCDCF_ON | LCDCF_OBJON | LCDCF_BGON
	ldh [rLCDC], a
	ei

	; Jump to tank mode main routine.
	ld hl, BANK("Shrimp Tank Mode")
	call ChangeBank
	jp TankModeMain

; @param de: source map.
; @param bc: map size.
TankModeScrnCopy:
	ld hl, _SCRN0
	:
		ld a, [de]        ; Copy [DE]->[HL]
		ld [hli], a
		ld a, [wbIndexer] ; wbIndexer++
		inc a
		cp SCRN_X_B       ; If offscreen zero out a tile and goto nex
		jr c, :+
			ld a, 0       ; Write a zero tile into VRAM and skip to next line.
			ld [hli], a
			push de
			ld de, SCRN_VX_B - SCRN_X_B - 1
			add hl, de
			pop de
		:
		ld [wbIndexer], a ; Update variables.
		inc de
		dec bc
		ld a, b
		or c
	jr nz, :--
	ret

SECTION "Detail Mode Change Routine", ROM0

ChangeModeDetail:

	; TODO: Load assets for details mode.

	; Switch back to standard speed.
	call SwitchSpeed

	; Turn LCDC back on
	ld a, LCDCF_ON | LCDCF_OBJON | LCDCF_BGON
	ldh [rLCDC], a
	ei

	; Jump to details mode main routine.
	; ld hl, BANK("Details Mode")
	; call ChangeBank
	; jp DetailsModeMain

SECTION "Pause Mode Change Routine", ROM0

ChangeModePause:

	; TODO: Load assets for pause mode.

	; Switch back to standard speed.
	call SwitchSpeed

	; Turn LCDC back on
	ld a, LCDCF_ON | LCDCF_OBJON | LCDCF_BGON
	ldh [rLCDC], a
	ei

	; Jump to pause mode main routine.
	; ld hl, BANK("Pause Mode")
	; call ChangeBank
	; jp PauseModeMain

SECTION "Mode Variables", WRAM0

wbCurMode:: db ; Current game mode.