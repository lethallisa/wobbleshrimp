; vim:tw=80:ts=4

INCLUDE "hardware.inc"
INCLUDE "font.inc"

; ******************************************************************************
; License screen display routine.
; ******************************************************************************

SECTION "License Screen", ROM0

ShowLicense::
	; Wait for VBLANK.
	rst RstWaitUntilVBlank

	; Turn LCD off.
	ld a, LCDCF_OFF
	ldh [rLCDC], a

	; Render license data to SCRN0
	ld de, LicenseData
	ld bc, LicenseData.end - LicenseData
	ld hl, BANK("License Screen")
	call RenderText

	; Turn LCD back on.
	ld a, LCDCF_ON | LCDCF_BGON
	ldh [rLCDC], a
	: ; Input processing:

		; Do input while in vblank
		rst RstWaitUntilVBlank

		; Wait for user to press start.
		call DoInput
		ldh a, [hCurKeys]
		bit PADB_START, a
	jr z, :-
	ret

LicenseData:
	db "\n"
	db "    WOBBLESHRIMP\n"
	db "       DEMO\n"
	db "\n"
	db "(C)2024 GALE FARADAY\n"
	db "\n"
	db " NOT LICENSED BY\n"
	db " NINTENDO.\n"
	db " SOURCE AVAILABLE\n"
	db " UNDER BSD 3-CLAUSE\n"
	db " LICENSE\n"
	db "\n"
	db "\n"
	db "    PRESS START%"
.end: