; vim:tw=80:ts=4

; ******************************************************************************
; Tile Data
; ******************************************************************************

SECTION "Font Tiles", ROMX

FontTiles::
	INCBIN "tiles/font.2bpp"
.end::

FontPal::
	INCBIN "tiles/font.pal"
.end::

SECTION "Tank Mode Tiles", ROMX

TankTiles::
	INCBIN "tiles/tanktiles.2bpp"
.end::

TankPal::
	INCBIN "tiles/tanktiles.pal"
.end::

TankTileMap::
	INCBIN "tiles/tankmode.tilemap"
.end::

TankAttrMap::
	INCBIN "tiles/tankmode.attrmap"
.end::