; vim:tw=80:ts=4:ft=asm

; ******************************************************************************
; Input Macros and Definitions
; ******************************************************************************

; Default value for input delay frames.
DEF INPUT_DELAY_COUNT EQU 15
