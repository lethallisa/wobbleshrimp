## Makefile for Wobbleshrimp

## -----------------------------------------------------------------------------
## Set phony & default targets, and override the default suffix rules.
## -----------------------------------------------------------------------------

.PHONY: all clean cleangfx test release
.SUFFIXES:

.DEFAULT_GOAL := all

## -----------------------------------------------------------------------------
## Project Defaults & Folders
## -----------------------------------------------------------------------------

TARGET   := wobbleshrimp
TARGROM  := $(TARGET).gbc
RELEASE  := $(TARGET)-release.zip
SOURCES  := src/
BUILD    := build/
DATA     := $(SOURCES)data/
OBJS     := $(patsubst $(SOURCES)%.s,$(BUILD)%.o,$(wildcard $(SOURCES)*.s))
PICS     := $(patsubst %.png,%.2bpp,$(wildcard $(DATA)pics/*.png))
TILES    := $(patsubst %.png,%.2bpp,$(wildcard $(DATA)tiles/*.png))
SPRITES  := $(patsubst %.png,%.2bpp,$(wildcard $(DATA)sprites/*.png))
GFX      := $(PICS) $(TILES) $(SPRITES)
PALETTES := $(patsubst %.2bpp,%.pal,$(GFX))

LICENSEE ?= "GF"
TITLE    ?= "WOBBLSHRIMP"
GAMEID   ?= "WSWS"
GAMEREV  ?= 0
MAPPER   ?= 0x1B
CARTRAM  ?= 0x02

## -----------------------------------------------------------------------------
## Set flags and programs used to build.
## -----------------------------------------------------------------------------

AS       := rgbasm
LD       := rgblink
FIX      := rgbfix
GT       := rgbgfx
EMU      ?= sameboy
ZIP      ?= zip

INCDIRS  := -I $(SOURCES) -I $(DATA)
ASFLAGS  := $(INCDIRS)
LDFLAGS  := -d -w
FIXFLAGS := -C -v -j -p 0xFF \
			-m $(MAPPER) -r $(CARTRAM) \
			-l 0x33 -k $(LICENSEE) \
			-t $(TITLE) -i $(GAMEID) -n $(GAMEREV)
GTFLAGS  := -d 2 -u -P

ifdef DEBUG
	LDFLAGS += -n $(TARGET).sym
endif

## -----------------------------------------------------------------------------
## Compilation Rules
## -----------------------------------------------------------------------------

## Generate release archive.
release: $(RELEASE)

## Zip release (requires infozip).
%.zip: $(TARGROM)
	$(ZIP) $@ $^
	sha256sum -b $@ > $@.sha256

## 7zip release (requires p7zip).
%.7z: $(TARGROM)
	7z a $@ $^
	sha256sum -b $@ > $@.sha256

## Test ROM.
test: $(TARGROM)
	$(EMU) $<

## Convert graphics.
gfx: $(GFX)

## Convert picture tiles.
$(PICS): %.2bpp : %.png
	$(GT) $(GTFLAGS) -T -b 1 -o $@ $<

## Convert background tiles.
$(TILES): %.2bpp : %.png
	$(GT) $(GTFLAGS) -o $@ $<

## Convert sprite tiles.
$(SPRITES): %.2bpp : %.png
	$(GT) $(GTFLAGS) -o $@ $<

## Compile all.
all: $(GFX) $(TARGROM)

## Link and fix.
$(TARGROM): $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^
	$(FIX) $(FIXFLAGS) $@

## Assemble objects.
$(OBJS): $(BUILD)%.o : $(SOURCES)%.s
	-@mkdir -p $(BUILD)
	$(AS) $(ASFLAGS) -o $@ $<

## Remove $(BUILD) dir.
.IGNORE: clean
clean:
	@echo 'Cleaning up intermediary files...'
	@rm -rvf $(BUILD) $(TARGROM) $(TARGET).sym

## Remove $(GFX) files.
.IGNORE: cleangfx
cleangfx:
	@echo 'Cleaning up graphics files...'
	@rm -rvf $(GFX) $(PALETTES)